# Livrable 3 pour la mission avec les Ateliers Amasco
## Description de l'application
### Contexte de la mission

Il s'agit d'une mission de conseil bénévole réalisée avec Alter'Actions. Amasco est une association d'éducation populaire qui propose des ateliers pédagogiques et ludiques à des enfants de tous milieux.

Soucieux de s'exporter au national, ils sont pour le moment essentiellement implantés en région parisienne et dans l'Est. Il nous fallait réaliser une étude sur les villes de Rennes et de Nantes pour estimer l'intérêt d'un éventuel déploiement dans l'Ouest.

### Étude socio-économique

Une part importante de la mission résidait dans l'étude des données socio-économiques de la région Rennaise (répartition des âges, revenus, etc...).

Cette étape de la mission a fait l'objet d'un livrable statique comprenant divers cartes réalisées à l'aide du module `geopandas`. Et une application web interactive réalisée à partir de widgets Python.

### Source des données

Les données dans `amasco_action3` sont issues de l'open Data de l'INSEE.

## Serveur de développement et déploiement

### Serveur de développement local

#### Création de l'environnement virtuel avec pyenv-virtualenv

`scripts/bootstrap` 

#### Lancement de l'application en local

`voila cartography.ipynb`

### Déploiement

Le déploiement a été réalisé à l'aide d'Heroku. L'application est accessible [ici](https://amasco-action3.herokuapp.com/).
